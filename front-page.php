<?php
/**
 * Template Name: FrontPage
 *
 * @package WordPress
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$args = array(
    'post_type' => 'post'
);
$context['post'] = Timber::get_posts($args);


Timber::render( array( 'front-page.twig', 'page.twig' ), $context );
