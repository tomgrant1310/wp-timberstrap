<?php
/**
 * Template Name: Property Description
 *
 * @package WordPress
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$args = array(
    'post_type' => 'property'
);
$context['property'] = Timber::get_posts($args);

$unitValue = get_field( "unit_id" );

// echo '<pre>';
// print_r($unitValue);
// echo '</pre>';

$table = $wpdb->prefix . 'anytime_availability';
$found = false;
$query   = "SELECT unit_id, level FROM {$wpdb->prefix}anytime_availability";
$results = $wpdb->get_results($query);

// echo '<pre>';
// print_r($results);
// echo '</pre>';

$found   = false;
foreach ($results as $eachResult) {
    if (intval($eachResult->unit_id) == $unitValue) {
        $found = true;
        echo '<pre>';
        print_r($eachResult->level);
        echo '</pre>';
    }
}

$availability = Timber::get_context();
Timber::render( array( 'single-property.twig', 'page.twig' ), $context );


