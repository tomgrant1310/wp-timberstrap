import Vue from 'vue'
import Router from 'vue-router'

//components

import Bookings from '../components/Bookings'
import Placebooking from '../components/Placebooking'
Vue.use(Router)

const router = new Router({
    routes: [
      {
        path: '/bookings',
        name: 'Bookings',
        component: Bookings
      },
      {
        path: '/place-booking',
        name: 'Placebooking',
        component: Placebooking
      }
    ]
  })

  export default router