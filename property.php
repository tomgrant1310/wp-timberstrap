<?php
/**
 * Template Name: Property
 *
 * @package WordPress
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$args = array(
    'post_type' => 'property',
);
$context['property'] = Timber::get_posts($args);


Timber::render( array( 'property.twig', 'page.twig' ), $context );