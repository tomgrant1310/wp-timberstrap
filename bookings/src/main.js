import 'babel-polyfill'
import 'babel-plugin-root-import'
import Vue from 'vue'
import App from './App.vue'
// import BootstrapVue from 'bootstrap-vue'
// Vue.use(BootstrapVue);
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import router from './router'
import VueCurrencyFilter from 'vue-currency-filter'

Vue.use(VueCurrencyFilter,
  {
    symbol : '£',
    thousandsSeparator: '.',
    fractionCount: 2,
    fractionSeparator: ',',
    symbolPosition: 'front',
    symbolSpacing: true
  })

Vue.use(require('vue-moment'));

Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken1, // #E53935
    secondary: colors.lime.lighten1, // #d4e157
    accent: colors.indigo.base // #3F51B5
  }
})
new Vue({
  el: '#wp-vue-app',
  router,
  // components: { App },
  render: h => h(App)
})