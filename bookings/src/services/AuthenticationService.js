import Api from "@/services/Api"
// we now have an API object from the above

export default {
  // containing a method called register to hit the endpoint
  authorise(credentials) {
    return Api().post('/authorise/', credentials, {
      headers: {
        Accept: 'application/json;version=1.0'
      }
    })
  },
  bookings(token, from_date, to_date, filter_selected) {
    const headerData = {
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json;version=1.0'
      }
    };
    const postData = {
      from_date: from_date,
      to_date: to_date,
      unit: 9408,
      selected: filter_selected
    }
    return Api().post('/booking/', postData, headerData)
  },
  getUnits(token, unit_id) {
    const headerData = {
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json;version=1.0'
      }
    }
    return Api().get('/unit/' + unit_id, headerData)
  },
  getRates(token, unit_id) {
    const headerData = {
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json;version=1.0'
      }
    }
    const postData = {
      unit_id: [unit_id]
    }
    console.log(unit_id + token)
    return Api().post('/price/', postData, headerData)
  }
}
