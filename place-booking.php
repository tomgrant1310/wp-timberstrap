<?php
/**
 * Template Name: Booking page
 *
 * @package WordPress
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$args = array(
    'post_type' => 'post',
);
$context['post'] = Timber::get_posts($args);


Timber::render( array( 'place-booking.twig', 'page.twig' ), $context );