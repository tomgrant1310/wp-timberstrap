import axios from 'axios'

export default () => {
  if (process.env.NODE_ENV == 'production') {
    return axios.create({
      baseURL: `https://api.anytimebooking.eu/`
    })
  } else {
    return axios.create({
      baseURL: `http://localhost:3000`
    })
  }
}
// can be used to hit our end points
